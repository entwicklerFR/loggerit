![npm](https://img.shields.io/npm/v/loggerit)
![npm](https://img.shields.io/npm/dt/loggerit)  
[![pipeline status](https://gitlab.com/entwicklerFR/loggerit/badges/master/pipeline.svg?style=flat)](https://gitlab.com/entwicklerFR/loggerit/commits/master)
[![coverage report](https://gitlab.com/entwicklerFR/loggerit/badges/master/coverage.svg)](https://gitlab.com/entwicklerFR/loggerit/commits/master)

# loggerit
Typescript node package that wraps the `node global console`.
- Simple to use
- For Typescript

## Technical notes
- Written in Typescript and compiled in ECMAScript with target version ES2015 (aka ES6)
- Node.js >= 8.0.0

## Level
Six different levels with the following weight order
- TRACE: prints to `stdout`
- DEBUG: prints to `stdout`
- INFO: prints to `stdout`
- WARN: prints to `stderr`
- ERROR: prints to `stderr`
- FATAL: prints to `stderr`

And a special one
- NONE: prints nothing to nowhere

## Installation
```
$ npm install --save loggerit
```

## Typescript example
### Import
```js
import { Logger, Level } from 'loggerit';
```
### INFO level
```js
const logger: Logger = new Logger(Level.INFO);

logger.write(Level.TRACE, 'trace');
logger.write(Level.DEBUG, 'debug');
logger.write(Level.INFO, 'info');
logger.write(Level.WARN, 'warn');
logger.write(Level.ERROR, 'error');
logger.write(Level.FATAL, 'fatal');
```
### Console result
Only INFO, WARN, ERROR and FATAL logs are written  
![](https://gitlab.com/entwicklerFR/loggerit/raw/master/media/logs-1.png)
### NONE level
```js
logger.setLevel(Level.NONE);

logger.write(Level.TRACE, 'trace');
logger.write(Level.DEBUG, 'debug');
logger.write(Level.INFO, 'info');
logger.write(Level.WARN, 'warn');
logger.write(Level.ERROR, 'error');
logger.write(Level.FATAL, 'fatal');

```
### Console result
No logs
```
```

## From string to Level type
```js
const level: Level = (<any>Level)['INFO'];
//const level: Level = (Level as any)['INFO'];
```

## From Level to string type
```js
const stringLevel: string = Level[Level.INFO];
```
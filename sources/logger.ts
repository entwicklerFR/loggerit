enum Level {
  TRACE,
  DEBUG,
  INFO,
  WARN,
  ERROR,
  FATAL,
  NONE,
}

enum Color {
  TRACE = '\x1b[32m',
  DEBUG = '\x1b[34m',
  INFO = '\x1b[36m',
  WARN = '\x1b[33m',
  ERROR = '\x1b[35m',
  FATAL = '\x1b[31m',
  NONE = '\x1b[37m',
}

class Logger {
  private _level: Level;

  constructor(level: Level) {
    this._level = level;
  }

  public setLevel = (level: Level): void => {
    this._level = level;
  };

  public getLevel = (): Level => {
    return this._level;
  };

  public write = (level: Level, message?: string, ...rest: any): void => {
    if (!message) {
      message = '';
    }

    if (level >= this._level) {
      const stringLevel: string = Level[level];
      const levelColor: Color = (Color as any)[stringLevel];
      const data: string = `[${levelColor}${stringLevel}${Color.NONE}] ${new Date().toISOString()} ${message}`;

      // tslint:disable: no-console
      switch (level) {
        case Level.TRACE: {
          console.trace(data, ...rest);
          break;
        }
        case Level.DEBUG: {
          console.debug(data, ...rest);
          break;
        }
        case Level.INFO: {
          console.info(data, ...rest);
          break;
        }
        case Level.WARN: {
          console.warn(data, ...rest);
          break;
        }
        case Level.ERROR: {
          console.error(data, ...rest);
          break;
        }
        case Level.FATAL: {
          console.error(data, ...rest);
          break;
        }
        default: {
          // Level.NONE: nothing written
          break;
        }
      }
      // tslint:enable: no-console
    }
  };
}

export { Level, Logger };

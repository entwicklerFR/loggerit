import { Logger, Level } from '../sources/logger';

const logger: Logger = new Logger(Level.INFO);

logger.write(Level.TRACE, 'trace');
logger.write(Level.DEBUG, 'debug');
logger.write(Level.INFO, 'info');
logger.write(Level.WARN, 'warn');
logger.write(Level.ERROR, 'error');
logger.write(Level.FATAL, 'fatal');
logger.write(Level.NONE, 'none');

/*
Console result:
[INFO] 2019-11-06T14:32:04.075Z OCC-ADMIN info
[WARN] 2019-11-06T14:32:04.077Z OCC-ADMIN warn
[ERROR] 2019-11-06T14:32:04.077Z OCC-ADMIN error
[FATAL] 2019-11-06T14:32:04.077Z OCC-ADMIN fatal
[NONE] 2019-11-06T14:32:04.077Z OCC-ADMIN none
*/

logger.setLevel(Level.NONE);

logger.write(Level.TRACE, 'trace');
logger.write(Level.DEBUG, 'debug');
logger.write(Level.INFO, 'info');
logger.write(Level.WARN, 'warn');
logger.write(Level.ERROR, 'error');
logger.write(Level.FATAL, 'fatal');

/*
Console result:
*/